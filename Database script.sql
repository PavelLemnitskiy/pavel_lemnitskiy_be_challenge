create database Biarri;

CREATE SCHEMA staging;
CREATE SCHEMA rostering;

CREATE TABLE rostering.employee
(
    employeeid integer NOT NULL,
    firstname character varying COLLATE pg_catalog."default",
    lastname character varying COLLATE pg_catalog."default",
    CONSTRAINT employee_pkey PRIMARY KEY (employeeid)
);


CREATE TABLE rostering.shift
(
    shiftid integer NOT NULL,
    date date NOT NULL,
    startat time without time zone NOT NULL,
    endat time without time zone NOT NULL,
    break integer NOT NULL DEFAULT 60,
    CONSTRAINT shift_pkey PRIMARY KEY (shiftid)
);


CREATE TABLE rostering.timetable
(
    timetableid integer NOT NULL,
    employeeid integer,
    shiftid integer,
    comment character varying COLLATE pg_catalog."default",
    CONSTRAINT timetable_pkey PRIMARY KEY (timetableid),
    CONSTRAINT timetable_employeeid_fkey FOREIGN KEY (employeeid)
        REFERENCES rostering.employee (employeeid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT timetable_shiftid_fkey FOREIGN KEY (shiftid)
        REFERENCES rostering.shift (shiftid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);


