import DatabaseConnection
import csv
from flask import jsonify


class DataInterface(object):
    def importEmployee(self, path):
        cn = DatabaseConnection.DatabaseConnection()

        if not (cn is None):
            seen = []
            with open(path, 'r') as csvfile:
                reader = csv.reader(csvfile)
                next(reader)  # Skip the header

                # Only unique values
                for row in reader:
                    if row in seen: continue
                    seen.append(row)
                    cn.queryModify(
                        "INSERT INTO rostering.employee(FirstName, LastName) VALUES ('%s', '%s')" % (row[0], row[1]))

    def importShift(self, path):
        cn = DatabaseConnection.DatabaseConnection()

        if not (cn is None):
            seen = []
            with open(path, 'r') as csvfile:
                reader = csv.reader(csvfile)
                next(reader)  # Skip the header

                # Only unique values
                for row in reader:
                    if row in seen: continue
                    seen.append(row)
                    cn.queryModify(
                        "INSERT INTO rostering.shift(Date, StartAt, EndAt, Break) VALUES (to_date('%s', 'DD/MM/yyyy'), '%s', '%s', %s)" % (
                            row[0], row[1], row[2], row[3]))

    def importData(self, employeePath, shiftPath):
        cn = DatabaseConnection.DatabaseConnection()

        if not (cn is None):
            cn.queryModify('truncate table rostering.timetable')
            cn.queryModify('delete from rostering.shift')
            cn.queryModify('delete from rostering.employee')

            self.importEmployee(employeePath)
            self.importShift(shiftPath)

    def addEmployee(self, FirstName, LastName):
        cn = DatabaseConnection.DatabaseConnection()

        if not (cn is None):
            # Find the employee
            result = cn.querySelect(
                "select EmployeeId from rostering.employee where FirstName = '%s' and LastName = '%s'" % (
                    FirstName, LastName))
            if result is not None and len(result) > 0:
                return "The employee already exists"

            cn.queryModify(
                "INSERT INTO rostering.employee(FirstName, LastName) VALUES ('%s', '%s')" % (FirstName, LastName))
            return "The employee has been added"

    def addShift(self, Date, StartAt, EndAt, Break):
        cn = DatabaseConnection.DatabaseConnection()

        if not (cn is None):
            # Find the shift
            result = cn.querySelect(
                "select ShiftId from rostering.shift where Date = to_date('%s', 'DD/MM/yyyy') and Startat = '%s' and Endat = '%s'" % (
                    Date, StartAt, EndAt))
            if result is not None and len(result) > 0:
                return "The shift already exists"

            cn.queryModify(
                "INSERT INTO rostering.shift(Date, StartAt, EndAt, Break) VALUES (to_date('%s', 'DD/MM/yyyy'), '%s', '%s', %s)" % (
                    Date, StartAt, EndAt, Break))
            return "The shift has been added"


    def addTimetable(self, FirstName, LastName, Date, StartAt, EndAt, Comment):
        cn = DatabaseConnection.DatabaseConnection()

        if not (cn is None):
            # Find the employee
            result = cn.querySelect(
                "select EmployeeId from rostering.employee where FirstName = '%s' and LastName = '%s'" % (
                    FirstName, LastName))
            if result is None or len(result) == 0:
                return "Employee not found"
            if len(result) > 1:
                return "More than one employee found"
            employeeId = result[0][0]

            # Find the shift
            result = cn.querySelect(
                "select ShiftId from rostering.shift where Date = to_date('%s', 'DD/MM/yyyy') and Startat = '%s' and Endat = '%s'" % (
                    Date, StartAt, EndAt))
            if result is None or len(result) == 0:
                return "Shift not found"
            if len(result) > 1:
                return "More than one shift found"
            shiftId = result[0][0]

            # Removing (if exists) the previous version, as there may be a new comment, and add a new row
            cn.queryModify(
                "delete from rostering.timetable where EmployeeId = %s and ShiftId = %s" % (employeeId, shiftId))
            cn.queryModify("insert into rostering.timetable (EmployeeId, ShiftId, Comment) values (%s, %s, '%s')" % (
                employeeId, shiftId, Comment))

            return "Timetable has been updated, EmployeeeId: %s, ShiftId: %s" % (employeeId, shiftId)


    def readTimeTable(self, FirstName, LastName):
        cn = DatabaseConnection.DatabaseConnection()

        if not (cn is None):
            # Find the employee
            result = cn.querySelect(
                "select EmployeeId from rostering.employee where FirstName = '%s' and LastName = '%s'" % (
                    FirstName, LastName))
            if result is None or len(result) == 0:
                return "Employee not found"
            if len(result) > 1:
                return "More than one employee found"
            employeeId = result[0][0]

            result = cn.querySelect(
                "select s.ShiftId, s.Date, s.StartAt, s.EndAt, s.Break, t.Comment from rostering.shift s inner join rostering.timetable t on t.ShiftId = s.ShiftId where t.EmployeeId = %s order by s.Date, s.StartAt" % (
                    employeeId))

            payload = []
            content = {}
            for r in result:
                content = {"ShiftId": r[0], "Date": r[1], "StarTAt": str(r[2]), "EndAt": str(r[3]), "Break": r[4], "Comment": r[5]}
                payload.append(content)
                content = {}
            print(jsonify(payload))
            return jsonify(payload)
