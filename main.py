import DataInterface
import os
from flask import Flask
from flask import request


app = Flask(__name__)


# This method removes all existing daa from the database and upload the mock data from CSV
@app.route('/importdata', methods=['POST'])
def importdata():
    d = DataInterface.DataInterface()
    d.importData(os.path.join(os.path.dirname(__file__), 'Data\employees.csv'), os.path.join(os.path.dirname(__file__), 'Data\shifts.csv'))
    return 'Database updated'


# This method adds an employee (json: {"FirstName":"Ivan","LastName":"Ivanov"})
@app.route('/addemployee', methods=['POST'])
def addemployee():
    content = request.get_json()
    d = DataInterface.DataInterface()
    return d.addEmployee(content['FirstName'], content['LastName'])


# This method adds a shift (json: {"Date":"01/10/2018","Startat":"9:00:00 PM","Endat":"5:30:00 AM","Break":"60"})
@app.route('/addshift', methods=['POST'])
def addshift():
    content = request.get_json()
    d = DataInterface.DataInterface()
    return d.addShift(content['Date'], content['Startat'], content['Endat'], content['Break'])


# This method adds a timetable (json: {"FirstName":"Frank","LastName":"White","Date":"20/06/2018","Startat":"9:00:00 PM","Endat":"5:30:00 AM","Comment":"manually updated or so..."})
@app.route('/addtimetable', methods=['POST'])
def addtimetable():
    content = request.get_json()
    d = DataInterface.DataInterface()
    return(d.addTimetable(content['FirstName'], content['LastName'], content['Date'], content['Startat'], content['Endat'], content['Comment']))


# This method reads a timetable (json: {"FirstName":"Frank","LastName":"White"})
@app.route('/readtimetable', methods=['POST'])
def readtimetable():
    content = request.get_json()
    d = DataInterface.DataInterface()
    return(d.readTimeTable(content['FirstName'], content['LastName']))



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

