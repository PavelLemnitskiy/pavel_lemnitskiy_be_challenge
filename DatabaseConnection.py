import configparser
import psycopg2
import os


class DatabaseConnection(object):
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = object.__new__(cls)

            # Reading the config
            config = configparser.ConfigParser()
            config.read(os.path.join(os.path.dirname(__file__), "config.ini"))
            print(config.sections())
            try:
                print('connecting to the PostgreSQL database...')
                connectionString = "host='{}' dbname='{}' user='{}' password='{}'". format(
                    config.get('DatabaseConnection', 'host'),
                    config.get('DatabaseConnection', 'dbname'),
                    config.get('DatabaseConnection', 'user'),
                    config.get('DatabaseConnection', 'password')
                )
                print(connectionString)
                connection = DatabaseConnection._instance.connection = psycopg2.connect(connectionString)

                cursor = DatabaseConnection._instance.cursor = connection.cursor()
                cursor.execute('SELECT VERSION()')
                db_version = cursor.fetchone()

            except Exception as error:
                print('Error: connection not established {}'.format(error))
                DatabaseConnection._instance = None

            else:
                print('connection established\n{}'.format(db_version[0]))

        return cls._instance

    def __init__(self):
        self.connection = self._instance.connection
        self.cursor = self._instance.cursor

    def queryModify(self, query):
        try:
            result = self.cursor.execute(query)
            self.connection.commit()
        except Exception as error:
            print('error execting query "{}", error: {}'.format(query, error))
            return None
        else:
            return result

    def querySelect(self, query):
        try:
            self.cursor.execute(query)
            result = self.cursor.fetchall()
        except Exception as error:
            print('error execting query "{}", error: {}'.format(query, error))
            return None
        else:
            return result

    def __del__(self):
        self.connection.close()
        self.cursor.close()