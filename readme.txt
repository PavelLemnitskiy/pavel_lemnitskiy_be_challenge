IDE: PyCharm
OS: Windows (should work in any environment though)
Type: web-service, Flask

Other tools:
 - Postman to test the web-service functions;
 - Docker to run the Postgres container;
 - Shell with the psql utility


How to start:
 - Create the database, using the "Database script.sql" script;
 - Edit the config.ini file, providing details for connecting to the database (I used Postgres);
 - Run the project;
 - The web-service will be available on the localhost: 127.0.0.1:5000


Web-methods:

1. importdata
Type: POST
Input parameters: None
Removes all existing data from the database and fills it by unique rows from the csv files. The files should be located in the "Data" subfolder of the project's folder.
Returns: text.


2. addemployee
Type: POST
Input parameters: JSON
Sample: {"FirstName":"Ivan","LastName":"Ivanov"}
Adds an employee into the database, if it still doesn't exist.


3. addshift
Type: POST
Input parameters: JSON
Sample: {"Date":"01/10/2018","Startat":"9:00:00 PM","Endat":"5:30:00 AM","Break":"60"}
Adds a shift into the database, if it still doesn't exist.


4. addtimetable
Type: POST
Input parameters: JSON
Sample: {"FirstName":"Frank","LastName":"White","Date":"20/06/2018","Startat":"9:00:00 PM","Endat":"5:30:00 AM","Comment":"manually updated or so..."}
Adds a timetable for the specified employee and shift into the database. If the timetable already exists, rewrites.


5. readTimeTable
Type: POST
Input parameters: JSON
Sample: {"FirstName":"Frank","LastName":"White"}
Returns all timetables for the specified person in JSON formst:
[
  {
    "Break": 60,
    "Comment": "manually updated or so...",
    "Date": "Mon, 18 Jun 2018 00:00:00 GMT",
    "EndAt": "05:30:00",
    "ShiftId": 402,
    "StarTAt": "21:00:00"
  },
  {
    "Break": 60,
    "Comment": "manually updated or so...",
    "Date": "Tue, 19 Jun 2018 00:00:00 GMT",
    "EndAt": "05:30:00",
    "ShiftId": 405,
    "StarTAt": "21:00:00"
  },
  {
    "Break": 60,
    "Comment": "manually updated or so...",
    "Date": "Wed, 20 Jun 2018 00:00:00 GMT",
    "EndAt": "05:30:00",
    "ShiftId": 408,
    "StarTAt": "21:00:00"
  }
]




The ideas for the future:
 - Improve the data validation and the errors handling parts;
 - Implement an additional data layer: add PL functions for reading/updating data, grant privileges on these functions to the app's account and hide the actual data structure from everyone, except the admin group. It'd improve the security significantly, as well as prevent from different kinds of SSL injections etc. 
 - Add a user-friendly web-interface;
 - Implement the staging functionality and attach a proper ETL process which can read csv files from different locations, put them into the staging schema and add to the main schema properly after;
 - Some statistics for the timetables;
 - Resources levelling and resource planning (i.e. no more than 40 hours per week, no more than 8 hours in consequent 24 etc);
 - A report for all employees altogether;
 - Improve security of the app.



 The questions:
 1. Who will be using that app? Do we need a system of privileges (so, an ordinary employee can only see and manage his own timesheets, a manager can see all his subordinates etc)?
 2. What is the timesheets policy? Are there closed periods etc?
 3. Interface to interact with a payroll system, with Expensify etc.
 4. Who is responsible for maintaining the database?
 5. How is it supposed to manage the organizational structure? Should it be imported from another system (payroll???), or should it be implemented right here?
 6. Import/export of data.
 7. Working in different timezones, overtime working.
 8. Budgeting: contract/permanents, different rates etc. Should it be in the system, and if the answer is positive, on which level?